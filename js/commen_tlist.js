$(function() {

    let id
    function get_commen_tlist(pageIndex = 1, pageSize = 10) {
        $.get( '/admin/comment/search', { page: pageIndex, perpage: pageSize }, function (resData) {
            // 调用art-templte模板渲染数据
            let trString = template('art_comment', resData.data);
            $('.table tbody').html(trString);
    

                   // 初始化分页控件
        init_pager(pageIndex,resData.data.totalCount,pageSize)
  
        })
    }

    get_commen_tlist()


    // 初始化分页控件
function init_pager(currPage,totalCount, pageSize) {
    layui.use('laypage', function () {
        var laypage = layui.laypage;

        laypage.render({
            elem: 'pager', 
            count: totalCount, //数据总数，从服务端得到
            limit: pageSize ,// 每页显示条数
            curr:currPage,  // 当前页码
            jump: function (obj, first) {
                //obj包含了当前分页的所有参数，比如：
                //console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                //console.log(obj.limit); //得到每页显示的条数

                //注意get_comment_list首次不执行，否则会出现调用死循环
                if (!first) {
                    // 调用分页获取评论数据方法，将新的页码和每页显示的数据传入进去
                    get_commen_tlist(obj.curr, obj.limit);
                }
            }
        });
    });
}


// 给所有的批准按钮注册点击事件
$('tbody').on('click', '.btnAccept',function () {

    const id = $(this).attr('data-id')
    console.log(id)
    $.post('/admin/comment/pass',{id},function(resData){
        console.log(resData)
        if (resData.code == 200) {
            layer.alert('是否通过？')

            get_commen_tlist()
        }
      
      
    })
})
// 在拒绝元素上注册onclick事件

$('tbody').on('click', '.btnReject',function () {

    const id = $(this).attr('data-id')
    console.log(id)
    $.post('/admin/comment/reject',{id},function(resData){
        console.log(resData)
        if (resData.code == 200) {
            layer.alert('是否拒绝？')
            
            get_commen_tlist()
        }
    })
})
// 在删除元素上注册onclick事件
// $('tbody').on('click', '.btnDelete',function () {

    // const id = $(this).attr('data-id')
    // console.log(id)
    // $.post('/admin/comment/pass',{id},function(resData){
    //     console.log(resData)
    //     if (resData.code == 200) {
    //         layer.alert('是否删除？')
            
    //         get_commen_tlist()
    //     }
    // })




    // $.ajax({
    //     url:'/admin/comment/pass',
    //     type:'post',
    //     data:{id:id},
    //     success(res) {
    //         console.log(res)
    //         if (res.code == 200) {

    //             layer.alert(res.msg)
    //             get_commen_tlist()
                
    //         }
    //     }
    // })
// })


$('tbody').on('click','.btnDelete', function() {
    // 5.1 弹出窗口 询问用户
// this 当前的按钮 delete   this 指向的问题  this指向谁 看 谁在调用它 ！！！

// 箭头函数   
layer.confirm('是否确认删除？', (index) =>{
  // // this 指向！！！！ 

  const id = $(this).attr('data-id')
  $.post("/admin/comment/delete", {id} , function (res) {
  

   // 5.2 刷新数据
   get_commen_tlist()
   layer.close(index)
  })
})

})
})