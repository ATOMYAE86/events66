/* 


1 初始化表单内的组件 
  1 复制粘贴 ！！ 

2 通过 location和URLSearchParams 获取传递过来的id
3 根据id 发送请求获取被编辑的文章详情数据

4 给发布按钮绑定点击事件
  1 获取对应的数据 拼接成 formdata格式的参数
  2 发送给后台 完成编辑
  3 编辑成功了 重定向回文章列表页面 ！！ 




 */

$(function () {



    // 全局变量 文章id
  let id;
    // const usp=new URLSearchParams(this.location.search);
    // const id=usp.get("id");
  
    // $.get("/admin/article/search",{id},function (res) {
    //   console.log(res);
    // })
  
  
    // 1 文件上传框 绑定 change事件
    $("#inputCover").change(function () {
      const file = this.files[0];
      $("#coverimg").prop("src", URL.createObjectURL(file));
    })
    // 2 发送请求 获取 文章列表数据
    function getCategoryList() {
      $.get("/admin/category/list", function (res) {
        let html = "";
        res.data.forEach(value => {
          html += `<option value="${value.id}" >${value.name}</option>`;
        });
        $("#category").html(html);
      })
    }
  
    // 3 初始化 日期选择器
    function renderDate() {
      //执行一个laydate实例
      layui.laydate.render({
        elem: '#articleDate' //指定元素
      });
    }
  
    // 4 初始化富文本编辑器
    function renderEditor() {
      tinymce.init({
        selector: '#articleContent',
        language: 'zh_CN',//注意大小写
      });
    }

    // 5 发送请求 获取 数据 渲染到 表单组件中
   function getArticleDetail() {
       const usp = new URLSearchParams(this.location.search) 
    //    console.log(usp)
    id = usp.get('id')
    $.get("/admin/article/search",{ id },function (res) {
      console.log(id);

      const { title, cover, categoryId, date, content } = res.data;

         // 5.1 填充 标题
         $("[name='title']").val(title);
         $("#coverimg").prop("src", cover);
         $("[name='categoryId']").val(categoryId);
         $("[name='date']").val(date);
         // 文章内容  1  直接给文本域赋值  2 如果失败了 查阅 tinymce文档 查看如何赋值 
         $("#articleContent").val(content);
   

    })

   }


    getCategoryList();
    renderDate();
    renderEditor();
    getArticleDetail();

     // 6  给 “发布” 绑定点击事件
     $('.btn-edit').click(function() {

        postArticle('已发布')
     })
       //  6 给 “草 稿” 绑定点击事件
       $('.btn-draft').click(function() {

        postArticle('')
     })


     function postArticle(state) {

      const fd = new FormData($("#form")[0]);

          // 对应富文本的内容 需要单独添加到formdata中
          const content = tinyMCE.editors['articleContent'].getContent()
          fd.append('content',content)
          fd.append('state', state)
          fd.append('id',id)

            //  fd.forEach((v,k)=>console.log(v,k));

             $.ajax({
                 url:"/admin/article/edit",
                 type:'post',
                 data:fd,
                 contentType:false,
                 processData:false,
                 success(res) {
                     if(res.code === 200) {
                        // 5.3 返回文章列表页面
                        location.href = 'article_list.html' 
                     }
                 }
             })



     }
  })