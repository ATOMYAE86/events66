/* 
1 发送请求 获取文章列表数据 （参数自己的理解 写下来）
  1 为了后期方便修改 请求参数 定义成全局变量 
  2 定义函数 发送请求 获取数据 渲染页面 art-template 来渲染页面

2 动态渲染 文章类别 下拉框 

3 动态渲染 分页组件 

4 点击分页组件 
  1 修改全局参数 
  2 发送一次请求 OK 

5 点击 “筛选”  按钮
  1 获取表单的值
  2 发送到后台 即可  完成数据筛选 
  3 bug
    思考
      1 数据没有做 筛选的时候 总页数 22页 
      2 先点击了 分页 "22页"  正常显示 
      3 再点击 筛选按钮 "已发布" 显示异常
        发送后台的参数
        1 页码  22页
        2 状态  已发布 

      4 例子
        1 班级里面人数  10排 
        2 显示第10排的同学  显示正常
        3 第10排队 同学 状态 秃头！！ 
      5 之前的分页的数据 总页数22页   先思考 没有筛选 再做分页
      6 现在做了筛选了 
        应该先筛选 数据 再统计 他们 总页数 

6 重置
  1 绑定点击事件
  2 重置 
    1 请求参数 params 
    2 搜索表单 
    3 分页组件  
    4 页面数据 重新刷新 。。。。

    7 发布文章

8 编辑文章

9 删除文章 
  1 给 按钮 绑定点击事件  委托！！ 
  2 弹出确认框
  3 执行删除
  4 刷新数据 


 */

  $(function() {

      // 1.1 请求参数 全局变量
  let params = {
    // string	搜索关键词，可以为空，为空返回某类型所有文章
    key: "",
    // string	文章类型id，可以为空，为空返回所有类型文章
    type: "",
    // string	文章状态，草稿 ，已发布,为空返回所有状态文章
    state: "",
    // number	当前页，为空返回第1页
    page: 1,
    // number	每页显示条数，为空默认每页6条
    perpage: 10
  }

    // 1.2 发送请求获取文章列表数据 /admin/article/query
    function articleQuery() {

        $.get('/admin/article/query',params,function (res) {
             console.log(res)
            const html = template('trTpl', { list:res.data.data})
            $('tbody').html(html)

            renderPager(res.data.totalCount);
        })
    }

  // 2 发送请求 获取 文章列表数据 /admin/category/list
function getCategoryList() {

    $.get('/admin/category/list',function(res) {
        console.log(res)

        let html = "<option value='' >所有类型</option>";
        res.data.forEach(value => {
            html += `<option value="${value.id}" >${value.name}</option>`;

        })
        $('#selCategory').html(html)

    })
}

  // 3 动态渲染分页组件
  function renderPager(count) {
    layui.laypage.render({
      elem: 'pager',
      count,
      // 默认 每一页的数据条数 10,
      limit: params.perpage,
      // 显示页码 
      curr:params.page,
      // 点击页码事件
      jump(obj, first) {
        //首次不执行
        if (!first) {
          // 获取被点击的页码
        //   console.log(obj.curr);
          params.page = obj.curr;
          articleQuery();

        }
      }
    });

  }

   // 4  给筛选按钮 绑定点击事件
 $('#btnSearch').click(function () {
  // 标题
  params.key = $("#aname").val();
  // 文章类型
  params.type = $("#selCategory").val();
  // 发布状态
  params.state = $("#selStatus").val();

  // 有bug  往往都需要 重置 分页参数  
  // 一旦 实现筛选功能 往往都需要 重置 页码 
  params.page = 1;
  // 发送请求 获取对应的数据
  articleQuery();


 })
  // 5 给重置按钮 绑定点击事件
 $('#btnReset').click(function() {
   // 5.1 请求参数
    params = { key:'', type:'', state:'', page:1, perpage: 10}

    // 5.2 搜索表单 
  $("#aname").val('');
  // 文章类型
$("#selCategory").val('');
  // 发布状态
 $("#selStatus").val('');
    // 5.3 重置分页组件  不需要做  因为在 获取文章列表的时候 内部 已经重置 组件
    // 5.4 刷新页面
 articleQuery();
 })
  
  // 6 删除按钮 delete  委托
  $("tbody").on("click", ".delete", function () {
    // console.log(12345);
    //  console.log('%c'+"大大的打印","color:red;font-size:100px;background-image:linear-gradient(to right,#0094ff,pink)");


    // console.log(id);
    layer.confirm("您确定要删除吗", (index) => {
      layer.close(index);
      const id = $(this).parents("tr").data("id");
      $.post("/admin/article/delete", { id }, function (res) {
        // console.log(id);
        if (res.code === 200) {
          // 刷新数据
          articleQuery();
        }

      })


    })

  })

    articleQuery();
    getCategoryList();
  })