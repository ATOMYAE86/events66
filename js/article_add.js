$(function () {
    /* 
      
    1 图片上传 即时显示
    
    2 动态渲染 文章列表下拉框
    
    3 渲染 layui日期选择器
  
    4 渲染 富文本编辑器 
    

     5 点击发布 
    1 获取对应的表单的参数 
    2 拼接成 接口 规定 数据格式 
    3 发送请求 完成新增
    4 返回 文章列表页面 （）

      
    */
  
  
    // 2 发送请求 获取 文章列表数据 /admin/category/list
    function getCategoryList() {
      $.get("/admin/category/list", function (res) {
        let html = "<option value='' >所有类型</option>";
        res.data.forEach(value => {
          html += `<option value="${value.id}" >${value.name}</option>`;
        });
        $("#category").html(html);
      })
    }
  
  
    // 3 初始化 日期选择器
    function renderDate() {
      //执行一个laydate实例
      layui.laydate.render({
        elem: '#articleDate' //指定元素
      });
    }
  
    // 4 初始化富文本编辑器
    function renderEditor() {
      tinymce.init({
        selector: '#articleContent',
        language:'zh_CN',//注意大小写
      });
      
    }
  
    // 1 文件上传框 绑定 change事件
    $("#inputCover").change(function () {
        // 由于是单选图片，所以取files数组中的第一个元素就是我们选择的图片对象
    const file = this.files[0];
      $("#coverimg").prop("src", URL.createObjectURL(file)); // 会返回 浏览器内存中的图片的地址 
    })
  
  
    // 2 发送请求 获取 文章列表数据 /admin/category/list
    getCategoryList();
  
    // 3 初始化 日期选择框
    renderDate();
  
    // 4 初始化 富文本编辑器
    renderEditor();
  
  

//  // 5  给发布按钮 绑定点击事件 /admin/article/publish
//      // formData 主要是用在文件上传 
//   $(".btn-edit").click(function () {
//     // 5.1 创建了一个formdata对象
//     const fd = new FormData($("#form")[0]);

//     // 5.2.1 获取文章标题
//     const title = $("[name='title']").val();
//     // 5.2.2 获取 图片文件 
//     // $("#inputCover")[0] === this
//     const cover = $("[name='cover']")[0].files[0];

//     // 5.2.3 获取 文章列表id
//     const categoryId = $("[name='categoryId']").val();

//     // 5.2.4 获取日期
//     const date = $("[name='date']").val();




//     // 5.2.5 获取富文本的内容
//     const content = tinyMCE.editors["articleContent"].getContent();


//     // console.log(title);
//     // console.log(cover);
//     // console.log(categoryId);
//     // console.log(date);
//     // console.log(content);

//     fd.append("title", title);
//     fd.append("cover", cover);
//     fd.append("categoryId", categoryId);
//     fd.append("date", date);
//     fd.append("content", content);
//     fd.append("state", "已发布");

//     fd.forEach((value,key)=>console.log(value,key));

//     // 5.2.3    因为我们之前有学习过 只不过 忘记了遗忘了！！ 
//     // ajax post请求 把 fd formData 发给后台即可 
//     // $.post("/admin/article/publish",fd,function (res) {
//     //   console.log(res);
//     // })

//     $.ajax({
//       url:"/admin/article/publish",

  
//   })
// })

 // 5  给发布按钮 绑定点击事件 /admin/article/publish
  // formData 主要是用在文件上传 
  $(".btn-edit").click(function () {
    postArticle("已发布");
  })

  // 5 给 “草稿” 绑定点击事件
  $(".btn-draft").click(function () {
    postArticle("");
  })


  // 6 新增文章或者新增草稿
  function postArticle(state) {
 
      // 5.1 创建了一个formdata对象
      const fd = new FormData($("#form")[0]);
  
  
      // 5.2.5 获取富文本的内容
      const content = tinyMCE.editors["articleContent"].getContent();
  
      fd.append("content", content);
      console.log(content)
      fd.append("state", state );
      console.log(state)
  
      fd.forEach((value,key)=>console.log(value,key));
  
      // 5.2.3    因为我们之前有学习过 只不过 忘记了遗忘了！！ 
  
      $.ajax({
        url:"/admin/article/publish",
        type:"post",
        data:fd,
        contentType:false,
        processData:false,
        success(res){
          // console.log(res);
          if(res.code===200){
            // 5.3 返回文章列表页面
            location.href="article_list.html";
          }
        }
      })
    
  }


})