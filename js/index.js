
// 1 获取用户信息 渲染到页面上
// 2 退出功能
// 3 左侧菜单 点击
  // 3.1 一级标题点击 
  // 3.2 二级标题点击




$(function () {

    // $.ajax({
    //   url:"/admin/user/info",
    //   success(res){
    //     console.log(res);
    //   }
    // })
  
  function getUserInfo () {

        // 写这段代码的时候 自动的携带token过去
        $.get("/admin/user/info",function (res) {
            //   console.log(res);
          
      const { nickname, userPic } = res.data;
      $(".user_info img").prop("src", userPic)
      $(".user_info span").text("欢迎  " + nickname);

      $(".user_center_link img").prop("src", userPic)
            
            })
  }
  getUserInfo();
    

  
  // 2 绑定退出按钮的点击事件
  $(".logout").click(function () {
    layer.confirm('您确定要删除吗😭', function (index) {
        // 退出
      layer.close(index);
    //   清除缓存
      localStorage.removeItem("token");
    //   跳转到登录页面
      location.href = "login.html";
    });
  })

  // 3 给左侧一级菜单绑定点击事件
  $(".level01").click(function () {
    // 1 切换选中 
    $(this).addClass("active").siblings().removeClass("active");
    // 2 控制箭头切换方向 
    $(this).find("b").toggleClass("rotate0");
    // 3 让它下一个兄弟元素 切换显示  stop!! 
    $(this).next(".level02").stop().slideToggle();
  })

  // 3.2 给二级标题绑定点击 切换选中
  $(".level02 li").click(function () {
    // 怎么知道 有active 类 
    // 废话 代码是我写
    $(this).addClass("active").siblings().removeClass("active");
  })

  })