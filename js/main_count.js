$(function() {
    // 获取顶部区域4组数据
    function get_data_info() {
        $.get( '/admin/data/info', null, function (resData) {
            let data = resData;
            /*data格式：
                {
                    "totalArticle": 223,
                    "dayArticle": 18,
                    "totalComment": 7999,
                    "dayComment": 36
                }
            */
            $('.totalCount em').text(data.totalArticle)
            $('.totalCount_day em').html(data.dayArticle)
            $('.commentCount em').html(data.totalComment)
            $('.commentCount_day em').html(data.dayComment)
        })
    }


    // 获取日新增文章数据并渲染echars折线图
    function get_new_article_chars() {
        $.get('/admin/data/article', null, function (resData) {
            let data = resData.date;
            // 生成日新增文章折线图
            // 1.0 根据页面dom对象初始化echarts对象，dom是存放echars的容器
           
            const dayChars = echarts.init($('#day_article_show')[0]);

            // 2.0 设置echars参数
            // 折线图配置参考：https://echarts.apache.org/examples/zh/editor.html?c=line-smooth
            let options = {
                //2.0.1 设置echars顶部标题
                title: {
                    text: '日新增文章趋势', // 标题文字
                    left: 'center' //让标题居中
                },
                // 2.0.2 设置图表类型以及设置图标显示的数据
                series: [{
                    name: '日增长数值',
                    type: 'line', // line类型必须设置xAxis,yAxis
                    smooth: true, // 开启折线的平滑处理
                    data: data.map(item => item.count),
                    label: {
                        normal: {
                            show: true,
                            position: 'top'
                        }
                    },
                    // 2.0.6 设置区域样式
                    areaStyle: {
                        color: {
                            type: 'linear',
                            x: 0,
                            y: 0,
                            x2: 0,
                            y2: 1,
                            colorStops: [{
                                offset: 0, color: 'red' // 0% 处的颜色
                            }, {
                                offset: 1, color: '#ffffff' // 100% 处的颜色
                            }],
                            global: false // 缺省为 false
                        }
                    }
                }],
                // 2.0.3 设置图标x轴数据
                xAxis: {
                    name: '日期',
                    type: 'category', // category类型会自动从data属性中取数据作为x轴的值
                    boundaryGap: false,
                    data: data.map(item => item.date)  //来源于服务器json数据中的日期字段
                },
                // 2.0.3 设置y轴
                yAxis: {
                    name: '数量',
                    type: 'value'
                },
                // 2.0.4 设置鼠标放到轴上即显示数据详情
                tooltip: {
                    trigger: 'axis'
                },
                // 2.0.5 设置工具栏
                toolbox: {
                    show: true, //显示工具栏
                    feature: {
                        dataZoom: { //设置缩放
                            yAxisIndex: 'none' // 限制y轴缩放，只能X轴缩放
                        },
                        dataView: { readOnly: false }, // 允许编辑原始数据
                        magicType: { type: ['line', 'bar'] }, // 设置图表可以从line(折现图)和bar(柱状图)之前来回切换
                        restore: {},  // 设置重置数据
                        saveAsImage: {} // 设置保存图片
                    }
                }
            }


            // 3.0 根据参数生成ehcars图
            dayChars.setOption(options);
        })
    }



    // 获取分类饼状图
    function get_pie_show() {
        $.get('/admin/data/category', null, function (resData) {
            let data = resData.date;

            // 1.0 根据页面dom对象初始化echarts对象，dom是存放echars的容器
           
            const dayChars = echarts.init($('#pie_show')[0]);

            // 2.0 设置echars参数
            // 折线图配置参考：https://echarts.apache.org/examples/zh/editor.html?c=pie-nest
            let options = {
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b}: {c} ({d}%)'
                },
                // 设置图例
                legend: {
                    orient: 'horizontal', // 图例为水平方向
                    left: 10,
                    data: data.map(item => item.name)  // 图例的数据
                },
                // 调环装颜色
                color: ['#5885e8', '#13cfd5', '#00ce68', '#ff9565', '#20ff19'],
                series: [
                    {
                        name: '分类数量',
                        type: 'pie',
                        radius: ['40%', '55%'],                      
                        //  data只接受name和value属性名称的对象数组
                        data: data.map(item => ({ "name": item.name, "value": item.articles }))
                    }
                ]
            };

            // 3.0 根据参数生成ehcars图
            dayChars.setOption(options);
        })
    }



    // 获取柱状图
    function get_column_show() {
        $.get('/admin/data/article', null, function (resData) {
            let data = resData.date;

            // 1.0 根据页面dom对象初始化echarts对象，dom是存放echars的容器
           
            const dayChars = echarts.init($('#column_show')[0]);

            // 2.0 设置echars参数
         
            let options = {
                xAxis: {
                    type: 'category',
                    data: data.map(item => item.date)
                },
                yAxis: {
                    type: 'value'
                },
                tooltip: {
                    trigger: 'axis'
                },
                color: '#5885e8',
                series: [{
                    data: data.map(item => item.count),
                    type: 'bar',
                    barWidth: 25, // 设置柱子的宽度
                    showBackground: true,
                    backgroundStyle: {
                        color: 'rgba(220, 220, 220, 0.8)'
                    }
                }]
            };

            // 3.0 根据参数生成ehcars图
            dayChars.setOption(options);
        })
    }
    get_data_info()
    get_new_article_chars()
    get_pie_show()
    get_column_show()
})