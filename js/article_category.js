/* 
1 发送请求获取数据 渲染页面 - art-template  
2 给新增窗口的保存按钮绑定点击事件
 */



$(function() {

  // 全局变量 被编辑的数据的 id 
  let id;


    function getCategoryList() {

        $.get('/admin/category/list', function (res) {

            // console.log(res)
            const html = template('trTpl',{ list:res.data})
            console.log(html);
            $('.category_table tbody').html(html)
        })
    }

     // 1 发送请求 获取所有的分类数据
  getCategoryList();

  
//   2 给新增-窗口 保存 按钮 绑定点击事件
  $("#myModal .btn_opt").click(function () {
    // 2.1 获取 表单中的值(属性选择器)
    const name = $("input[name='name']").val().trim();
    const slug = $("input[name='slug']").val().trim();

    // 2.2 判断表单字段是否为空
    if (!name || !slug) {
      layer.alert("分类名或者分类别名不能为空");
      return;
    }

    // 2.3 发送请求 完成新增
    $.post("/admin/category/add", { name, slug }, function (res) {
      // console.log(res);
      // 2.4 关闭模态框 bootstrap 里面的 模态框关闭
      $('#myModal').modal('hide');
   // 2.5 清空新增表单中的值
   $("input[name='name']").val("")
   $("input[name='slug']").val("")
   // 2.6 刷新页面数据
   getCategoryList();

    })

  })

   // 3 编辑按钮(动态生成！！！  事件委托)
   $('.category_table').on('click','.edit', function() {
// 3.1  获取到被点击 那一行的数据 
    const obj = $(this).parents('tr').data('obj')

   // 3.2 渲染到 编辑模态框中 ！！ 
   $('#name').val(obj.name)
   $('#slug').val(obj.slug)

   id = obj.id

    
   })

   
  // 4  编辑  保存按钮
  $("#editModal .btn_opt").click(function () {
    // 4.1 获取被编辑数据 ｛id,name,slug｝
    // id ???
    const name = $("#name").val().trim();
    const slug = $("#slug").val().trim();
    // 4.2 发送post请求到后台 完成更新
    $.post("/admin/category/edit", { id, name, slug }, function (res) {
      // 4.3 成功之后  关闭模态框
      $("#editModal").modal("hide");
      // 4.4 刷新数据 。。
      getCategoryList();

    })

  })

    // 5 删除按钮(事件委托)
    $('.category_table').on('click','.delete', function() {
             // 5.1 弹出窗口 询问用户
    // this 当前的按钮 delete   this 指向的问题  this指向谁 看 谁在调用它 ！！！

       // 箭头函数   
       layer.confirm('是否确认删除？', (index) =>{
           // // this 指向！！！！ 

           const {id} = $(this).parents('tr').data('obj')
           $.post("/admin/category/delete", {id} , function (res) {

            // 5.2 刷新数据
            getCategoryList()
            layer.close(index)
           })
       })

    })

})