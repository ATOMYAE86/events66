/* 
1 使用 $.ajaxPrefilter 利用里面 option 简化的 url ！！ 
2 拦截器中 


 */
$(function () {
    $.ajaxPrefilter(function (option) {
  // 1 简化了url

  option.url = 'http://localhost:8080/api/v1' + option.url

   // 2 统一设置了 请求头中 携带 token
   option.headers = {
       Authorization: localStorage.getItem('token')
   }
   // 3 自动显示 加载中效果

//    加载开始前
   let loadIndex
   option.beforeSend = function () {
       loadIndex = layer.load( {time: 10*1000})

   }
// 加载开始后
option.complete = function() {
    layer.close(loadIndex)
}
    })
})